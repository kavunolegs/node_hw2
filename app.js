const express = require('express');
const app = express();
const mongoose = require('mongoose');
const config = require('config');
const {logs} = require('./middlewares/logs.middleware.js')

const PORT = process.env.PORT ?? config.get('PORT');

app.use(express.json());

app.use(`/api/auth`, logs, require('./routes/auth.router'));
app.use(`/api/users/me`, logs, require('./routes/users.router'));
app.use(`/api/notes`, logs, require('./routes/notes.router'));

const start = async () => {
  try {
    await mongoose.connect(config.get('mongoUrl'), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
    app.listen(PORT, () => {
      console.log(`server is running on ${PORT} port`);
    });
  } catch (e) {
    console.log('Server Error', e.message);
  }
};

start();
