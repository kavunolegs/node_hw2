const {Router} = require('express');
const config = require('config');
const router = Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const User = require('../models/User');

const validation = Joi.object({
  username: Joi.string()
      .alphanum()
      .min(3)
      .max(30)
      .required(),
  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
});

router.post('/register', async (req, res) => {
  try {
    const {username, password} = req.body;

    const validationRes = await validation.validateAsync({username, password});

    if ( validationRes.error ) {
      return res.status(400).json({
        message: validationRes.error,
      });
    }

    const person = await User.findOne({username});

    if (person) {
      return res.status(400).json({
        message: 'This user already exist',
      });
    }

    const hashedpassword = await bcrypt.hash(password, 12);

    const user = new User({username, password: hashedpassword});

    await user.save();
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


router.post('/login', async (req, res) => {
  try {
    const {username, password} = req.body;
    const validationRes = await validation.validateAsync({username, password});

    if ( validationRes.error ) {
      return res.status(400).json({
        message: validationRes.error,
      });
    }

    const user = await User.findOne({username});

    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).json({
        message: 'Wrong password. Please try agein',
      });
    }

    const token = jwt.sign(
        {userId: user.id},
        config.get('jwtSecret'),
        {expiresIn: '1h'},
    );

    res.json({message: 'Success', jwt_token: token});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  }
});

module.exports = router;
