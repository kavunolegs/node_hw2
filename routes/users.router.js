const {Router} = require('express');
const router = Router();
const auth = require('../middlewares/auth.middleware');
const User = require('../models/User');
const Note = require('../models/Note');
const bcrypt = require('bcryptjs');
const Joi = require('joi');

const validation = Joi.object({
  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
});

router.get('/', auth, async (req, res) => {
  try {
    const user = await User.findById({_id: req.user.userId}, {
      _id: 1,
      username: 1,
      createdDate: 1,
    });
    res.status(200).json({user});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  }
});

router.delete('/', auth, async (req, res) => {
  try {
    await User.findByIdAndDelete({_id: req.user.userId}, (err) => {
      if (err) {
        return res.status(400).json({message: e.message});
      }
    });
    await Note.deleteMany({userId: req.user.userId});
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  }
});

router.patch('/', auth, async (req, res) => {
  try {
    const {oldPassword, newPassword} = req.body;

    await validation.validateAsync({password: newPassword});
    const user = await User.findOne({
      _id: req.user.userId,
    });

    const isMatch = await bcrypt.compare(oldPassword, user.password);

    if (!isMatch) {
      return res.status(400).json({
        message: 'Wrong oldPassword. Please try agein',
      });
    }

    const hashedpassword = await bcrypt.hash(newPassword, 12);

    await User.updateOne(user, {password: hashedpassword});
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});

module.exports = router;
