const {Router} = require('express');
const router = Router();
const auth = require('../middlewares/auth.middleware');
const Note = require('../models/Note');

router.get('/', auth, async (req, res) => {
  try {
    const notes = await Note.find({userId: req.user.userId});
    res.status(200).json({notes});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  };
});

router.post('/', auth, async (req, res) => {
  try {
    const {text} = req.body;
    if (!text) {
      return res.status(400).json({message: 'There are no text'});
    }

    const note = new Note({
      userId: req.user.userId,
      text,
    });

    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  };
});

router.get('/:id', auth, async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      return res.status(400).json({message: 'Note not founded'});
    }
    const isUser = req.user.userId == note.userId;
    if (!isUser) {
      return res.status(400).json({message: 'Wrong user'});
    }

    res.status(200).json({note});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  };
});

router.put('/:id', auth, async (req, res) => {
  try {
    const {text} = req.body;
    if (!text) {
      return res.status(400).json({message: 'There are no text'});
    }

    const note = await Note.findById(req.params.id);
    if (!note) {
      return res.status(400).json({message: 'Note not founded'});
    }
    const isUser = req.user.userId == note.userId;
    if (isUser) {
      await Note.updateOne(note, {text});
      return res.status(200).json({message: 'Success'});
    }

    res.status(400).json({message: 'Wrong user'});
  } catch (e) {
    res.status(500).json({message: e.message});
  };
});

router.patch('/:id', auth, async (req, res) => {
  try {
    const note = await Note.findById(req.params.id);
    if (!note) {
      return res.status(400).json({message: 'Note not founded'});
    }

    const isUser = req.user.userId == note.userId;
    if (!isUser) {
      return res.status(400).json({message: 'Wrong user'});
    }

    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  };
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const note = await Note.findOne({_id: req.params.id});
    if (!note) {
      return res.status(400).json({message: 'Note not founded'});
    }

    const isUser = req.user.userId == note.userId;
    if (!isUser) {
      return res.status(400).json({message: 'Wrong user'});
    }

    await Note.deleteOne({_id: req.params.id});
    res.status(200).json({message: 'Success'});
  } catch (e) {
    res.status(500).json({message: 'Something go wrong'});
  };
});

module.exports = router;
